#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;

int serial_data = 0;
int N = 0;

void setup() {
  Serial.begin(115200);

  Wire.begin();

  sensor.setTimeout(500);
  if (!sensor.init())
  {
    Serial.println("Failed to detect and initialize sensor!");
    while (1) {}
  }
}

void loop() {
  if (Serial.available() > 0)
    serial_data = Serial.read();
  else
    serial_data = 0;

  if (serial_data == 's') N = 100;

  if (N > 0){
    Serial.print(sensor.readRangeSingleMillimeters());
    Serial.print(',');
    N--;
  }

  if (sensor.timeoutOccurred()) { Serial.println(" TIMEOUT"); }

  delay(10);
}

#include <Servo.h>

const int sensorIn = A0;
const int servoOut = 9;

int sensorValue = 0;        // value read from the pot

int serial_data = 0;
int print_en = 0;

void setup() {
  Serial.begin(115200);
}

void loop() {
  if (Serial.available() > 0)
    serial_data = Serial.read();
  else
    serial_data = 0;

  if (serial_data == 's') print_en = !print_en;

  sensorValue = analogRead(sensorIn);

  if (print_en) Serial.println(sensorValue);

  delay(10);
}

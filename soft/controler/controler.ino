#include <Servo.h>
#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;

const int servoOut = 9;
const int periodTest = 12;


volatile int next_cycle = 0;
int sensorValue = 0;        // value read from the pot

int serial_data = 0;

int print_en = 0;
int r_en = 0;

int x = 0;
int e_l = 0;
int u_0 = 103;
int z = 110;
int u;
int pid_p = 10;
int pid_d = 5;
int pid_2 = 0;
int use_kalman = 0;

int pid_u;

int xe_n[3];
int xe[3] = {100, 0, 0};
Servo servo;

void kalman(int xe_n[], int xe[], int x, int u) {
  long tmp;
  tmp = 804L*xe[0] + 50L*xe[1] + 196L*x;
  xe_n[0] = tmp/1000;
  tmp = -349L*xe[0] + 1000L*xe[1] + 487L*xe[2] + 349L*x;
  xe_n[1] = tmp/1000;
  tmp = 20L*xe[2] + 980L*u;
  xe_n[2] = tmp/1000;
}

int adc_to_mm(int x) {
  return (94*x-801)/100;
}

void setup() {
  Serial.begin(115200);
  servo.attach(servoOut);

  Wire.begin();

  sensor.setTimeout(500);
  if (!sensor.init())
  {
    Serial.println("Failed to detect and initialize sensor!");
    while (1) {}
  }

  pinMode(periodTest, OUTPUT);
  // 1 ms - 1000Hz
  cli();
  TCCR2A = 0;
  TCCR2B = 0;
  TCCR2B |= B00000100;
  TIMSK2 |= B00000100;
  OCR2B = 250;
  sei();
}

void loop() {
  // wait for cycle to start
  while (next_cycle == 0);
  next_cycle = 0;

  if (Serial.available() > 0)
    serial_data = Serial.read();
  else
    serial_data = 0;

  if (serial_data == 's') print_en = Serial.parseInt();
  if (serial_data == 'r') r_en = Serial.parseInt();
  if (serial_data == 'k') use_kalman = Serial.parseInt();
  if (serial_data == 'u') u_0 = Serial.parseInt();
  if (serial_data == 'z') z = Serial.parseInt();
  if (serial_data == 'p') pid_p = Serial.parseInt();
  if (serial_data == 'd') pid_d = Serial.parseInt();
  if (serial_data == 'f') pid_2 = Serial.parseInt();

  sensorValue = sensor.readRangeSingleMillimeters();
  if (sensor.timeoutOccurred()) { Serial.println(" TIMEOUT"); }

  x = adc_to_mm(sensorValue);

  kalman(xe_n, xe, x, pid_u);
  xe[0] = xe_n[0];
  xe[1] = xe_n[1];
  xe[2] = xe_n[2];

  int e = z - x;
  int de = e - e_l;
  e_l = e;

  if (!use_kalman)
    pid_u = (pid_p*e + pid_d*de) / 10;
  else
    pid_u = (pid_p*(z-xe[0]) - pid_d*xe[1] - pid_2*xe[2]) / 10;

  u = r_en ? u_0 - pid_u : u_0;
  
  u = u > 180 ? 180 :
      u < 0   ? 0 :
                u;

  pid_u = u_0 - u; // Kalman should take u with saturation
  
  servo.write(u);
  
  if (print_en) {
    Serial.print(pid_p);
    Serial.print(" ");
    Serial.print(pid_d);
    Serial.print(" ");
    Serial.print(z);
    Serial.print(" ");
    Serial.print(x);
    Serial.print(" ");
    Serial.print(xe[0]);
    Serial.print(" ");
    Serial.print(xe[1]);
    Serial.print(" ");
    Serial.print(xe[2]);
    Serial.print(" ");
    Serial.print(e);
    Serial.print(" ");
    Serial.print(pid_u);
    Serial.print(" ");
    Serial.print(u_0);
    Serial.print(" ");
    Serial.println(u);
  }

  if (next_cycle != 0) Serial.println("To long cycle");
  
}

// Timer2 interrupt
int period_test_state = 0;
int timer_count = 0;
ISR(TIMER2_COMPB_vect){
  TCNT2  = 0;
  timer_count++;
  if (timer_count == 50) {
    timer_count = 0;
    next_cycle = 1;
    period_test_state = !period_test_state;
    digitalWrite(periodTest, period_test_state);
  }
}

'use strict';

let port;
let reader;
let inputDone;
let outputDone;
let inputStream;
let outputStream;

const butConnect = document.getElementById('butConnect');
const divInfo = document.getElementById('info');

const taData = document.getElementById('taData');
const butClrData = document.getElementById('butClrData');


const cChartX = document.getElementById('cChartX');
var chartX;
const cChartU = document.getElementById('cChartU');
var chartU;
const cChartS = document.getElementById('cChartS');
var chartS;

var intervalSerial;

var exportData = {"labels": [], "data": []};
var currentData = [];

var setValue = 1;

document.addEventListener('DOMContentLoaded', () => {
  port = null;
  butConnect.addEventListener('click', clickConnect);
  butClrData.addEventListener('click', function() {taData.value="";});

  chartX = new Chart(cChartX, {
    type: 'line',
    data: {
      labels: [],
      datasets: [
        {
          label: 'x_e',
          data: [],
          borderWidth: 1
        },{
        label: 'x',
        data: [],
        borderWidth: 1
      },{
        label: 'z',
        data: [],
        borderWidth: 1
      }]
    },
    options: {
      animation: false,
      scales: {
        y: {
          beginAtZero: true
        },
        x: {
          ticks: {
            callback: function(value, index, ticks) {
              return value.toFixed(2);
            }
          }
        }
      },
      elements: {
        point:{
          radius: 0
        }
      }
    }
  });

  chartU = new Chart(cChartU, {
    type: 'line',
    data: {
      labels: [],
      datasets: [
        {
          label: 'u',
          data: [],
          borderWidth: 1
        }]
    },
    options: {
      animation: false,
      scales: {
        y: {
          beginAtZero: true
        },
        x: {
          ticks: {
            callback: function(value, index, ticks) {
              return value.toFixed(2);
            }
          }
        }
      },
      elements: {
        point:{
          radius: 0
        }
      }
    }
  });

  chartS = new Chart(cChartS, {
    type: 'line',
    data: {
      labels: [],
      datasets: [
        {
          label: 'v',
          data: [],
          borderWidth: 1
        },{
          label: 'xe_2',
          data: [],
          borderWidth: 1
        }]
    },
    options: {
      animation: false,
      scales: {
        y: {
          beginAtZero: true
        },
        x: {
          ticks: {
            callback: function(value, index, ticks) {
              return value.toFixed(2);
            }
          }
        }
      },
      elements: {
        point:{
          radius: 0
        }
      }
    }
  });

  if (!('serial' in navigator)) {
    setInfo(
      "Sorry, <b>Web Serial</b> is not supported on this device, make sure you're \
       running Chrome 78 or later and have enabled the \
       <code>#enable-experimental-web-platform-features</code> flag in \
       <code>chrome://flags</code>", "alert-danger");
   }
});

async function connect() {
  port = await navigator.serial.requestPort();
  await port.open({ baudRate: 115200 });

  let decoder = new TextDecoderStream();
  inputDone = port.readable.pipeTo(decoder.writable);
  inputStream = decoder.readable
    .pipeThrough(new TransformStream(new LineBreakTransformer()));

  reader = inputStream.getReader();

  const encoder = new TextEncoderStream();
  outputDone = encoder.readable.pipeTo(port.writable);
  outputStream = encoder.writable;

  //intervalSerial = window.setInterval(read_serial, 30);
  setValue = 1;
  read_serial();
}

async function disconnect() {
  if (reader) {
    await reader.cancel();
    await inputDone.catch(() => {});
    reader = null;
    inputDone = null;
  }
  if (outputStream) {
    await outputStream.getWriter().close();
    await outputDone;
    outputStream = null;
    outputDone = null;
  }
  await port.close();
  port = null;
  clearInterval(intervalSerial);
}

async function clickConnect() {
  if (port) {
    await disconnect();
    toggleUIConnected(false);
    return;
  }
  await connect();
  toggleUIConnected(true);
}

function writeToStream(...lines) {
  const writer = outputStream.getWriter();
  lines.forEach((line) => {
    console.log('[SEND]', line);
    writer.write(line + '\n');
  });
  writer.releaseLock();
}

function toggleUIConnected(connected) {
  let lbl = 'Connect';
  if (connected) {
    lbl = 'Disconnect';
    setInfo("Connected", "alert-success");
  } else {
    setInfo("Not connected", "alert-dark");
  }
  butConnect.textContent = lbl;
}

class LineBreakTransformer {
  constructor() {
    // A container for holding stream data until a new line.
    this.container = '';
  }

  transform(chunk, controller) {
    // CODELAB: Handle incoming chunk
    this.container += chunk;
    const lines = this.container.split('\n');
    this.container = lines.pop();
    lines.forEach(line => controller.enqueue(line));
  }

  flush(controller) {
    // CODELAB: Flush the stream.
    controller.enqueue(this.container);
  }
}

async function serial_send(cmd) {
  if (!port) {
    setInfo("Disconnected", "alert-warning");
    return;
  }
  setInfo("Sending: " + cmd, "alert-info");
  writeToStream(cmd);
}

var t = 0;
async function read_serial() {
    var d = 1;
    var iterMax = 1;
    var iter = iterMax;
    var maxLen = 50/iterMax*10;
    while(d) {
    const { value, done } = await reader.read();
    if (value) {
      //console.log(value);
      //divData.innerHTML
      var v = value.split(" ");
      var data = [];
      for (var i = 0; i < v.length; i++) {
        data.push(parseInt(v[i]));
      }
      taData.value += "[" + data.join(',') + "],";

      if (setValue == 1) {
        document.getElementById('inputZ').value = data[2];
        document.getElementById('inputP').value = data[0];
        document.getElementById('inputD').value = data[1];
        document.getElementById('inputU').value = data[9];
        setValue = 0;
      }

      iter--;
      if (iter > 0) continue;
      iter = iterMax;

      chartX.data.labels.push(t);
      chartX.data.datasets[0].data.push(data[4]);
      chartX.data.datasets[1].data.push(data[3]);
      chartX.data.datasets[2].data.push(data[2]);
      chartU.data.labels.push(t);
      chartU.data.datasets[0].data.push(data[8]);
      chartS.data.labels.push(t);
      chartS.data.datasets[0].data.push(data[5]);
      chartS.data.datasets[1].data.push(data[6]);
      
      if (chartX.data.labels.length > maxLen) {
        chartX.data.labels.shift();
        chartX.data.datasets[0].data.shift();
        chartX.data.datasets[1].data.shift();
        chartX.data.datasets[2].data.shift();
        chartU.data.labels.shift();
        chartU.data.datasets[0].data.shift();
        chartS.data.labels.shift();
        chartS.data.datasets[0].data.shift();
        chartS.data.datasets[1].data.shift();
      }

      t += 0.05*iterMax;
      chartX.update();
      chartU.update();
      chartS.update();
    }
    if (done) {
      console.log('[readLoop] DONE', done);
      reader.releaseLock();
      d = 0;
    }
  }
}

function setInfo(text, color) {
  divInfo.innerHTML = text;
  divInfo.className = "alert " + color;
}


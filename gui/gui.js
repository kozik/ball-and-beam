'use strict';

const getData = document.getElementById('getData');
const inputZ = document.getElementById('inputZ');
const inputP = document.getElementById('inputP');
const inputD = document.getElementById('inputD');
const inputX2 = document.getElementById('inputX2');
const inputU = document.getElementById('inputU');
const open = document.getElementById('open');
const PD = document.getElementById('PD');
const state = document.getElementById('state');

document.addEventListener('DOMContentLoaded', () => {
    getData.addEventListener('click', function() {
        var command = "s" + ((getData.checked) ? "1" : "0");
        serial_send(command);
    });

    inputZ.addEventListener('change', function() {
        var command = "z" + this.value;
        serial_send(command);
    });

    inputP.addEventListener('change', function() {
        var command = "p" + this.value;
        serial_send(command);
    });

    inputD.addEventListener('change', function() {
        var command = "d" + this.value;
        serial_send(command);
    });

    inputX2.addEventListener('change', function() {
        var command = "f" + this.value;
        serial_send(command);
    });

    inputU.addEventListener('change', function() {
        var command = "u" + this.value;
        serial_send(command);
    });

    open.addEventListener('click', function() {
        var command = "r0";
        serial_send(command);
    });

    PD.addEventListener('click', function() {
        var command = "k0r1";
        serial_send(command);
    });

    state.addEventListener('click', function() {
        var command = "k1r1";
        serial_send(command);
    });
});

